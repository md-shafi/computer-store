import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import Header from './Components/Header.js';
import Footer from './Components/Footer.js';
import Home from './Pages/Home';
import About from './Pages/About';
import Products from './Pages/Products';
import Product from './Pages/Product';

function App() {
  return (
    <div className='relative flex flex-col min-h-screen pb-20'>
      <Router>
        <Header />
        <div className='flex flex-col flex-grow p-4'>
          <Switch>
            <Route exact path='/'>
              <Home />
            </Route>
            <Route exact path='/products'>
              <Products />
            </Route>
            <Route path='/product/:id'>
              <Product />
            </Route>
            <Route path='/about'>
              <About />
            </Route>
          </Switch>
        </div>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
