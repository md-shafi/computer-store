import React from 'react'

export default function Footer() {
    return (
        <footer className="absolute bottom-0 bg-blue-800 w-full p-4 text-center text-xs sm:text-sm md:text-base font-normal text-white">
            &copy; Shafi 2020
        </footer>
    )
}