import React from 'react'

export default function Error() {
    return (
        <div className="flex flex-grow justify-center items-center">
            The page was not found :(
        </div>
    )
}
