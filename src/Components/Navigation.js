import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { useTransition, animated } from 'react-spring'
import NavMenu from './NavMenu'

export default function Navigation() {
    const [showMenu, setShowMenu] = useState(false)

    const menuOverlayTransitions = useTransition(showMenu, null, {
        from: { position: 'absolute', opacity: 0 },
        enter: { opacity: 1 },
        leave: { opacity: 0 },
    })

    const menuTransitions = useTransition(showMenu, null, {
        from: { opacity: 0, transform: 'translateX(-100%)' },
        enter: { opacity: 1, transform: 'translateX(0%)' },
        leave: { opacity: 0 }, transform: 'translateX(-100%)'
    })


    return (
        <nav>
            <FontAwesomeIcon icon={faBars} className='text-xl md:text-2xl lg:text-3xl' onClick={() => setShowMenu(!showMenu)} />

            {
                menuOverlayTransitions.map(({ item, key, props }) =>
                    item && <animated.div
                        key={key}
                        style={props}
                        className='bg-black bg-opacity-50 fixed top-0 left-0 w-full h-full z-10'
                        onClick={() => { setShowMenu(false) }}
                    >
                    </animated.div>
                )
            }

            {
                menuTransitions.map(({ item, key, props }) =>
                    item && <animated.div
                        key={key}
                        style={props}
                        className='bg-white fixed top-0 left-0 w-4/5 md:w-1/2 lg:w-1/4 h-full p-4 z-10 shadow'
                    >
                        <NavMenu showMenu={() => { setShowMenu(false) }} />
                    </animated.div>
                )
            }
        </nav>
    )
}
