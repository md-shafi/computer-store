import React from 'react'
import { Link } from 'react-router-dom'

export default function NavMenu({ showMenu }) {
    return (
        <div>
            <ul className='lg:text-lg font-normal font-normal text-blue-800'>
                <li>
                    <Link
                        to='/'
                        onClick={showMenu}
                        className='block py-4 border-b'
                    >
                        Home
                    </Link>
                </li>
                <li>
                    <Link
                        to='/products'
                        onClick={showMenu}
                        className='block py-4 border-b'
                    >
                        Products
                    </Link>
                </li>
                <li>
                    <Link
                        to='/about'
                        onClick={showMenu}
                        className='block py-4 border-b'
                    >
                        About
                    </Link>
                </li>
            </ul>
        </div>
    )
}
