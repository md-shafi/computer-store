import React from 'react'
import Navigation from './Navigation'

export default function Header() {
    return (
        <header className='flex justify-between items-center p-4 border-b-2 font-bold'>
            <div className='flex flex-col'>
                <span className="sm:text-xl md:text-2xl">Data Store</span>
                <span className='text-xs sm:text-base md:text-xl font-normal text-grey-300'>The bazar for hardware and software</span>
            </div>
            <Navigation />
        </header>
    )
}
