import React from 'react'
import { Link } from 'react-router-dom'
import Truncate from 'react-truncate';

export default function ProductCard({ product }) {
    return (
        <div className="rounded p-3 mb-3 overflow-hidden border sm:border-none sm:shadow-lg">
            <Link
                to={`product/${product.id}`}
            >
                <div className='relative pb-2/3 bg-blue-200'>
                    <img className='absolute h-full w-full object-cover' src={product.photo_url} alt={product.name} />
                </div>
                <div className="sm:h-16 mt-4">
                    <Truncate lines={2} ellipsis={<span>...</span>}>
                        <h3>{product.name}</h3>
                    </Truncate>
                </div>
                <div className='text-xl font-bold mb-4'>
                    € {product.price}
                </div>
                <button
                    className='bg-blue-800 w-full p-4 text-center text-xs sm:text-base text-white rounded'
                >
                    View
                </button>
            </Link>
        </div>
    )
}
