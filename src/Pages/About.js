import React from 'react'
import bgAboutPage from './../Img/about-bg-circuit.jpeg'

export default function About() {
    return (
        <div className="flex flex-col flex-grow bg-cover bg-center bg-no-repeat">
            <img src={bgAboutPage} className="max-h-half-screen object-cover" alt="electic circuit" />

            <div className="mt-8 text-center sm:text-base md:text-xl">
                We sell the best quality hardwares and softwares with an afordable price!
            </div>
        </div>
    )
}
