import React from 'react'
import useAxiosGet from '../Hooks/useAxiosGet'
import Loader from './../Components/Loader'
import Error from './../Components/Error'
import ProductCard from './../Components/ProductCard'


export default function Products() {
    let content = null

    const url = `https://jsonfy.com/items`
    let products = useAxiosGet(url)
    // let products = { loading: true }
    // let products = { error: true }

    if (products.loading) {
        content = <Loader />
    }

    if (products.error) {
        content = <Error />
    }

    if (products.data) {
        let contentHeading = <h1 className='sm:text-base text-center md:text-xl mt-4 mb-8'> Computer Softwares and Hardwares </h1>

        content = products.data.map((product) => {
            return (
                <div key={product.id}>
                    <ProductCard product={product} />
                </div>
            )
        })

        content =
            <>
                {contentHeading}
                <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
                    {content}
                </div>
            </>
    }

    return (
        <div className="flex flex-col flex-grow">
            {content}
        </div>
    )
}