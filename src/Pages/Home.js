import React from 'react'
import computerProductsImg from './../Img/home-hardwares.png'

export default function Home() {

    return (
        // <div className="flex-grow h-full bg-all-prod-icons bg-contain bg-center bg-no-repeat">
        <div className="flex flex-col flex-grow h-full">
            <h1 className='sm:text-base text-center md:text-xl mt-4 mb-8'> Computer hardware product categories</h1>
            <img className="max-h-half-screen object-contain" src={computerProductsImg} alt="computer hardwares" />
        </div>
    )
}
