import React from 'react'
import { useParams } from 'react-router-dom'
import Loader from './../Components/Loader'
import useAxiosGet from '../Hooks/useAxiosGet'

export default function Product() {
    const params = useParams()
    const url = `https://jsonfy.com/items/${params.id}`

    let product = useAxiosGet(url)

    let content = null

    if (product.loading) {
        content = <Loader />
    }

    if (product.error) {
        content = <div className="flex flex-grow justify-center items-center">The page was not found :(</div>
    }

    if (product.data) {
        content =
            <div className="lg:flex md:max-w-screen-sm lg:max-w-screen-md">
                <div className="flex-1">
                    <h1 className='lg:hidden text-base sm:text-2xl font-bold mt-4 md:mt-8'>
                        {product.data[0].name}
                    </h1>
                    <div className='p-3 mt-4 md:mt-8 w-5/6 sm:w-4/6 lg:w-5/6'>
                        <img src={product.data[0].photo_url} alt='product.data' />
                    </div>
                    <div className='lg:hidden font-bold mt-4 md:mt-8'>
                        {product.data[0].stock ? <span>In stock {product.data[0].stock}</span> : <span>Out of stock</span>}
                    </div>
                    <div className='lg:hidden text-xl font-bold mt-2 md:mt-4'>
                        € {product.data[0].price}
                    </div>
                </div>
                <div className="flex-1">
                    <h1 className='hidden lg:block text-xl font-bold mt-4 md:mt-8'>
                        {product.data[0].name}
                    </h1>
                    <div className="mt-4 md:mt-8">
                        {product.data[0].description}
                    </div>
                    <div className='hidden lg:block font-bold mt-4 md:mt-8'>
                        {product.data[0].stock ? <span>In stock {product.data[0].stock}</span> : <span>Out of stock</span>}
                    </div>
                    <div className='hidden lg:block text-xl font-bold mt-2 md:mt-4'>
                        € {product.data[0].price}
                    </div>
                </div>
            </div>
    }

    return (
        <div className="flex flex-grow justify-center">
            {content}
        </div>
    )
}
