import { useState, useEffect } from 'react'
import Axios from 'axios'

export default function useAxiosGet(url) {
    const [response, setResponse] = useState({
        loading: false,
        data: null,
        error: false
    })

    useEffect(() => {
        setResponse({
            loading: true,
            data: null,
            error: false
        })
        Axios.get(url)
            .then(response => {
                setResponse({
                    loading: true,
                    data: response.data,
                    error: false
                })
            }).catch(() => {
                setResponse({
                    loading: false,
                    data: null,
                    error: true
                })
            })
    }, [url])

    return response
}
